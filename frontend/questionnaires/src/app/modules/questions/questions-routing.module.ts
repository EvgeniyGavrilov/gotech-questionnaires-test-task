import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionsComponent } from './components/questions/questions.component';
import { QuestionResolverService } from './services/question-resolver.service';


const routes: Routes = [
  { 
    path: 'questions/:id', 
    component: QuestionsComponent,
    resolve: {
      question: QuestionResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule { }
