import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionsRoutingModule } from './questions-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { QuestionsComponent } from './components/questions/questions.component';




@NgModule({
  declarations: [
    QuestionsComponent,
  ],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    ReactiveFormsModule
  ]
})
export class QuestionsModule { }
