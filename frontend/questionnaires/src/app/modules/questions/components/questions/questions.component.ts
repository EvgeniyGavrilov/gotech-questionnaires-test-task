import { AfterContentChecked, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AnswerStringValue, Other, Question, QuestionList } from 'src/app/interfaces/questions';
import { QuestionsService } from '../../services/questions.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, OnDestroy, AfterContentChecked {
  public questionsList: QuestionList = {} as QuestionList;
  private unsubscribe$: Subject<null> = new Subject();

  profileForm = this.fb.group({
    subforms: this.fb.array([]) 
  });

  constructor(
    private fb: FormBuilder, 
    private questionsService: QuestionsService, 
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private router: Router
  ) {
    this.route.data.subscribe(data => {
        this.questionsList = data["question"];
        for(let question of this.questionsList.questions) {
          this.addSubforms(question);
        }
      }
    );
  }

  ngAfterContentChecked(): void {
    this.ref.detectChanges()
  }

  ngOnInit(): void {}

  subforms(): FormArray {
    return this.profileForm.get("subforms") as FormArray
  }


  
  newSubforms(q: Question): FormGroup {
    if(q.question.required) {
      return this.fb.group({
        result: ['', [Validators.required]],
        otherValue: [{ value: '', disabled: true }],
        subquestions: [''],
      });
    } else {
      return this.fb.group({
        result: [''],
        otherValue: [''],
        subquestions: [''],
      });
    }
  }

  addSubforms(q: Question) {
    this.subforms().push(this.newSubforms(q));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(null);
    this.unsubscribe$.complete();
  }

  getSubformByIndex(index: number): AbstractControl {
    return this.profileForm.get('subforms')?.get(String(index)) as AbstractControl;
  }

  result(index: number): string{
    return this.getSubformByIndex(index)?.get("result")?.value;
  }

  // valid(index: number): any{
  //   return {
  //     result: this.getSubformByIndex(index)?.get("result")?.errors,
  //     otherValue: this.getSubformByIndex(index)?.get("otherValue")?.errors,
  //     subquestions: this.getSubformByIndex(index)?.get("subquestions")?.errors
  //   };
  // }

  checkFn(answers: AnswerStringValue[], answer: AnswerStringValue ): boolean {
    return answers.includes(answer);
  }

  enableInput(index: number) {
    this.getSubformByIndex(index)?.get('otherValue')?.addValidators(Validators.required);
    this.getSubformByIndex(index)?.get('otherValue')?.enable();
  }

  resetInput(options: AnswerStringValue | Other, q: Question, index: number) {
    if(!this.getSubformByIndex(index)?.get("otherValue")?.disabled) {
      this.getSubformByIndex(index)?.get("otherValue")?.disable();
      this.getSubformByIndex(index)?.get("otherValue")?.removeValidators(Validators.required)
    }
    

    if(q.question.subquestions) {
      if(q.question.subquestions[options].required) {
        this.getSubformByIndex(index)?.get("subquestions")?.addValidators(Validators.required);
      } else {
        this.getSubformByIndex(index)?.get("subquestions")?.removeValidators(Validators.required);
      }
    }
  }

  onSubmit() {
    console.log('onSubmit');
  }
}
