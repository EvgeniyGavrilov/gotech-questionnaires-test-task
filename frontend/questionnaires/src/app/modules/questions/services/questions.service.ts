import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { QuestionList } from '../../../interfaces/questions'

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) { }

  public getQuestionsById(id: number): Observable<QuestionList> {
    return this.http.get<QuestionList>('http://localhost:3000/questionnaires/' + id)
  }
}
