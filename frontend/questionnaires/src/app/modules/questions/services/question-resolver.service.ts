import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { QuestionList } from 'src/app/interfaces/questions';
import { Injectable } from '@angular/core';
import { QuestionsService } from './questions.service';
@Injectable({
  providedIn: 'root'
})
export class QuestionResolverService implements Resolve<QuestionList>{

  constructor(private questionsService: QuestionsService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): QuestionList | Observable<QuestionList> | Promise<QuestionList> {
      return this.questionsService.getQuestionsById(Number(route.params["id"]));
  }
}
