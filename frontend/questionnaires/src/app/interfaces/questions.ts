export type AnswerTypes = 'multiple' | 'single' | 'text';
export type AnswerStringValue = string;
export type Other = "Other";

export type Questionnaires = {
    "questionnaires": QuestionList[]
}

export interface QuestionList { 
    "id": number,
    "questions": Question[] 
}

export interface Question {
    "id": number,
    "question": QuestionBody
}

interface QuestionBody {
    "required": boolean,
    "description": string,
    "answer": {
        "type": AnswerTypes,
        "options"?: (AnswerStringValue | Other)[]
    },
    "subquestions"?: {
        [key: AnswerStringValue]: QuestionBody
    }
}





